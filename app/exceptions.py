class CorruptedFileException(Exception):
    pass


class NotEnoughCreditException(Exception):
    pass

class RedisFailed(Exception):
    pass

class DBFailed(Exception):
    pass