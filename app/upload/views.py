from fastapi import Form, HTTPException, APIRouter,FileInput
from app.upload.service_layer.models import (
    DocumentResponse,
    Document,
    create_error_response
    )
from app.upload.service_layer.services import extract_metadata

router = APIRouter()

@router.post('/upload', response_model=DocumentResponse)
async def upload_file(file_input: FileInput = Form(...)):
    try:
        # Extract metadata from file
        metadata = await extract_metadata(file_input.file)
        document = Document(**metadata)
        return DocumentResponse(document)
    except HTTPException as e:
        return create_error_response(
            message = str(e),
            error_status = (500,"INVALID_INPUT"),
            translator = None
        )
    


