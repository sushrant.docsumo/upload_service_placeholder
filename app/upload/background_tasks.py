# app/upload/background_tasks.py

import json
import os

class BackgroundTaskManager:
    def __init__(self):
        self.local_file_path = "background_tasks.json"

    def send_to_background(self, task_data):
        """
        Sends the task data to background.
        If the task fails, stores it locally for retry.
        """
        try:
            # Example code to send task to background
            # background_service.send(task_data)
            print("Sending task to background:", task_data)
            # If successful, return True
            return True
        except Exception as e:
            print("Error sending task to background:", e)
            # If sending fails, store task locally
            self.store_locally(task_data)
            return False

    def store_locally(self, task_data):
        """
        Stores the task data locally for retry.
        """
        with open(self.local_file_path, "a") as f:
            f.write(json.dumps(task_data) + "\n")

    def bulk_upload_from_local(self):
        """
        Uploads tasks stored locally to background once it's back.
        """
        if os.path.exists(self.local_file_path):
            with open(self.local_file_path, "r") as f:
                for line in f:
                    task_data = json.loads(line.strip())
                    # Example code to send task to background
                    # background_service.send(task_data)
                    print("Sending task from local file to background:", task_data)
            # Clear local file after successful upload
            os.remove(self.local_file_path)