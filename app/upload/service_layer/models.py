from typing import List, Optional
from pydantic import BaseModel, validator
from pydantic.generics import GenericModel
import typing
from fastapi import UploadFile
from app.upload.validators import validate_base_64_url,validate_proper_file_url


ErrorDict = typing.Dict[str, typing.Any]
ErrorKeyTuple = typing.Tuple[int, str]

class Document(BaseModel):
    created_at: Optional[str]
    doc_id: str
    doc_meta_data: str
    email: str
    review_url: str
    status: str
    title: str
    file_type: str
    user_doc_id: str
    user_id: str


# Pydantic model for file input
class FileInput(BaseModel):
    file: typing.Union[UploadFile, str]
    filetype: str

    @validator('file')
    def determine_file_type(cls, value):
        if isinstance(value, UploadFile):
            return cls(file_type="file_upload")
        elif isinstance(value, str):
            if validate_base_64_url(value):
                return cls(file_type="base64")
            elif validate_proper_file_url(value):
                return cls(file_type="url")
            else:
                raise ValueError("Invalid file string format")
        else:
            raise ValueError("Invalid file type")

# Pydantic model for the response
class DocumentResponse(BaseModel):
    data: list[Document]
    error: str
    error_code: str
    message: str
    status: str
    status_code: int

class Error(BaseModel):
    code: int
    message: str
    error_key: str

class ErrorResponseModel(GenericModel):
    status: int
    error: Error
def create_error_response(
    *, message: str, error_status: ErrorKeyTuple, translator=None
) -> ErrorDict:
    #logger.error(message)
    if translator:
        message = translator(message)
    return ErrorResponseModel(
        status=error_status[0],
        error=Error(
            message=message,
            code=error_status[0],
            error_key=error_status[1],
        ),
    ).dict()



