from fastapi import UploadFile
from typing import Union
from app.upload.service_layer.unit_of_work import UnitOfWork
from app.upload.background_tasks import BackgroundTaskManager
from app.exceptions import DBFailed, RedisFailed

async def extract_metadata(file: Union[UploadFile, str]) -> dict:
    # Extract Metadata and Returns the result
    pass

async def get_user_credit(user)->dict:
    pass

async def decrease_user_credit(user)->dict:
    try:
        with UnitOfWork(db_uri) as session:
            user_repository = UserRepository(db_uri, db_name, collection_name)

            # Get a user by ID
            user_id = "user123"
            user = user_repository.get_user_by_id(user_id)
            if user:
                print("User found:", user)
            else:
                print("User not found")
                raise ValueError("User not found")

            # Update user credit
            updated_credit = 500
            success = user_repository.update_user_credit(user_id, updated_credit, session)
            if success:
                print("User credit updated successfully")
            else:
                raise ValueError("Failed to update user credit")
    except Exception as e:
        print(f"Transaction aborted: {e}")
        

async def send_file_to_background(file):
    background_handler = BackgroundTaskManager(file)
    try:
        background_handler.send_to_background(file)
    except RedisFailed, DBFailed:
        background_handler.store_locally(file,"instruction")
    

    