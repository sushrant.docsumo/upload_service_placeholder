class UnitOfWork:
    def __init__(self, db_uri):
        pass

    def __enter__(self):
        self.session.start_transaction()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.session.commit_transaction()
        else:
            self.session.abort_transaction()
        self.session.end_session()
    
